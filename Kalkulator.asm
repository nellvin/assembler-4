.data
	linia1: .asciiz "Podaj kod dzialania \n(1: dodawnie / 2: odejmowanie / 3: mnozenie / 4: dzielenie )"
	linia2: .asciiz "Podaj pierwszy argument "
	linia3: .asciiz "Podaj drugi argument "
	linia4: .asciiz "\nCzy chcesz wykonac kolejne dzialanie ? (1: tak / 0: nie )"
	linia5: .asciiz "Wynik: "

.text
	#Do p?tli
	poczatek:
	#Zapytanie o kod operacji 
	li $v0, 4	# kod do syscall #drukowanie tekstu
	la $a0, linia1	# co ma syscall wydrukowa?
	syscall

	#Pobranie kodu
	li $v0, 5	#pobieranie liczby calk
	syscall

	#Zapisanie kodu
	move $t1,$v0	#pseudo add: add $t1, $v0, $zero

	#Sprawdzenie kodu
	  #Mniejsze od 5
	addi $t4,$zero, 5
	slt $t5, $t1,$t4
	addi $t4, $zero, 1
	bne $t5, $t4,poczatek
	  #Wi?ksze od 0
	addi $t4,$zero, 1 
	slt $t5, $t1,$t4
	addi $t4, $zero, 0
	bne $t5, $t4,poczatek

	#Zapytanie o piersz? liczbe
	li $v0, 4
	la $a0, linia2
	syscall

	#Pobranie liczb
	li $v0, 7
	syscall

	#Zapisanie liczby
	#move $f2,$f0
	add.d $f2, $f0, $f30

	liczba2:
	#Zapytanie o drug?? liczbe
	li $v0, 4
	la $a0, linia3
	syscall

	#Pobranie liczb
	li $v0, 7
	syscall

	#Zapisanie liczby
	mov.d $f4,$f0

	#Wyzerowanie $t7
	add $t7, $zero, $zero

	#Dodawanie
	addi $t7,$t7,1
	bne $t1, $t7,odejmowanie	#jesli nie sa r�wne to skok
	add.d $f6, $f4, $f2

	odejmowanie:
	#Odejmowanie 
	addi $t7,$t7,1
	bne $t1, $t7,mnozenie		#jesli nie sa r�wne to skok
	sub.d $f6, $f2, $f4

	mnozenie:
	#Mno?enie
	addi $t7,$t7,1
	bne $t1, $t7,dzielenie		#jesli nie sa r�wne to skok
	mul.d $f6, $f4, $f2

	dzielenie:
	#Dzielenie
	addi $t7,$t7,1
	bne $t1, $t7,koniec		#jesli nie sa r�wne to skok
	c.eq.d $f4, $f30
	bc1t liczba2		#jesli druga liczba jest 0 to podaj jeszcze raz liczbe
	div.d $f6, $f2, $f4

	koniec:
	#Wy?wietlenie wyniku 
	li $v0, 4
	la $a0, linia5		#"Wynik: "
	syscall
	li $v0, 3		
	mov.d $f12,$f6 		#" *liczba* "
	syscall

	zapytanie:
	#Zapytanie o kolejne dzialanie
	li $v0, 4
	la $a0, linia4
	syscall

	#Pobranie kodu
	li $v0, 5
	syscall
		
	#Loop		Sprawdza czy jest 1 albo 0 
	addi $t0, $zero, 1
	beq $t0, $v0, poczatek
	add $t0, $zero, $zero
	bne $t0,$v0,zapytanie
